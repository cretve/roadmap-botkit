'use strict'

const Glue = require('glue');
const Manifest = require('./manifest');
const Config = require('./config');


var validate =  function (decoded, request, callback) {
  if (!people[decoded.id]) {
    return callback(null, false);
  }

  return callback(null, true, people[decoded.id]);
}

const registerAuth = function (server, next) {

  server.register([require('hapi-auth-jwt2')], (err) => {
    if (err) {
      console.log(err);
    }

    server.auth.strategy('jwt', 'jwt', {
      key: Config.get('/jwtSecret'),
      validateFunc: validate,
      verifyOptions: { algorithms: ['HS256'] }
    });

    server.auth.default('jwt');

  })

  return next();
}

const composeOptions = {
  relativeTo: __dirname,
  preRegister: registerAuth
}

Glue.compose(Manifest.get('/'), composeOptions, (err, server) => {


  server.start((err) => {
    if (err) {
      throw err;
    }

    console.log('Server is running now ......');
  });
});
