'use strict'

const Pack = require('../package');

const swaggerOptions = {
  info: {
    'title': 'Roadmap Botkit API Documentation',
    'version': Pack.version
  },
  securityDefinitions: {
    'jwt': {
      'type': 'apiKey',
      'name': 'Authorization',
      'in': 'header'
    }
  }
};

const helperPlugins = [
  {
    plugin: 'blipp'
  },
  {
    plugin: 'inert'
  },
  {
    plugin: 'vision'
  },
  {
    plugin: {
      register: 'hapi-swagger',
      options: swaggerOptions
    },
    options: {
      select: ['admin']
    }
  },      
];


module.exports = [
  ...helperPlugins,
  {
    plugin: './plugins/mailer'
  }
];
