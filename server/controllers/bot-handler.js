'use strict'

const apiai = require('apiai');
const app = apiai('577172eae3b047ef84df5246693c4b92');
const tokenHead = 'Bearer';
const tokenKey = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyNSwiaXNzIjoiaHR0cDpcL1wvc3R1ZGVudFwvYXBpXC9zdHVkZW50XC9sb2dpbiIsImlhdCI6MTQ5ODA5MjI5MiwiZXhwIjoxODEzNDUyMjkyLCJuYmYiOjE0OTgwOTIyOTIsImp0aSI6IjU5MDdhZDNhMjljODk2MjdkZTYyZGMwOWU4YjA1MGRhIn0.MxVQs-xkd8pQjWmOzKGkdscXZD4tVHcRR3MgCADxDCQ';

const botPostHandler = function(request, reply) {
    //console.log("rawPayload: " + request.rawPayload);
    //authorization check
    // console.log("Received POST from header=" + request.headers.authorization);

    //split the authorization hearer with OAuth 2.0 bearer protocal
    var authorization = request.headers.authorization;
    var token = authorization && authorization.split(' ');

    if(!token || token[0] !== tokenHead  || token[1] !== tokenKey)
    {
      reply({
        statusCode: 401,         
        error: 'Unauthorized authentication',
        message: request.payload.session,
      }).code(401);
      return;
    }

    console.log("Received POST from session=" + request.payload.session + "; text=" + (request.payload.text || 'Unknow'));

    var options = {
          sessionId: request.payload.session,
          lifespan: 720
    };

    var apiRequest = app.textRequest(request.payload.text, options);
  
    apiRequest.on('response',function(response) {
      reply({
        session:request.payload.session,
        reply: response.result.fulfillment.speech
      });
    });

    apiRequest.on('error',function(response) {
      reply({ 
        session:request.payload.session,
        reply: 'I\'m not totally sure about that.'
      });
    });

    apiRequest.end();  
}

module.exports = {
  botPostHandler: botPostHandler
};
