'use strict'

const routesArray = [
  {
    plugin: './server/routes/user-store.route',
    options: {
      select: ['admin'],
      routes: {
        prefix: '/admin'
      }
    }
  },
  {
    plugin: './server/routes/botkit.route',
    options: {
      select: ['admin'],
      routes: {
        prefix: '/bot'
      }
    }
  }
];

module.exports = routesArray;

