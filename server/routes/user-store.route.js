'use strict'

const Joi = require('joi');
const Config = require('../../config');

const getHandler = function (request, reply) {
  return reply('Success.');
}

const postHandler = function (request, reply) {
  return reply('Success.');
}

exports.register = function (server, options, next) {

  server.route([
    {
      method: 'GET',
      path: '/user/{userId}',
      config: {
        auth: Config.get('/authStrategy'),
        handler: getHandler,
        description: 'Retrieve a user',
        tags: ['api'],
        validate: {
          params: {
            userId: Joi.number().required().description('the user id'),
          },
        }        
      }
    }, {
      method: 'POST',
      path: '/user',
      config: {
        auth: Config.get('/authStrategy'),
        handler: postHandler,
        description: 'Create a user',
        tags: ['api'],
        validate: {
          payload: Joi.object({
            name: Joi.string().required(),
            email: Joi.string().required()
          }).required(),
        }
      }
    }
  ]);

  return next();
}

exports.register.attributes = {
  name: 'use-store',
  version: '1.0.0'
};
