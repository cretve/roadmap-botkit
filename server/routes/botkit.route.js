'use strict'

const Joi = require('joi');
const botPostHandler = require('../controllers/bot-handler').botPostHandler;

module.exports.register = function (server, options, next) {
  
  server.route([
    {
      method: 'POST',
      path: '/bot',
      config: {
        auth: false,
        handler: botPostHandler,
        description: 'Post request to API.ai',
        notes: 'Return a response message from the API.ai',
        tags: ['api'],
        validate: {
          payload: {
            session: Joi.string().required(),
            text: Joi.string().required()
          }
        }
      }
    },
  ]);

  return next();
}

module.exports.register.attributes = {
  name: 'botkit handler',
  version: '1.0.0'
}