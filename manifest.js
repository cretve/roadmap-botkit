'use strict'

const Confidence = require('confidence');
const Config = require('./config');

const criteria = {
  env: process.env.NODE_ENV
};

const pluginsArray = require('./plugins');

const routesArray = require('./server/routes');

const manifest = {
  $meta: 'This file defines the server',

  /* sever configuration */
  server: {
    debug: {
      request: ['error']
    },
    connections: {
      routes: {
        security: true,
        cors: true
      }
    }
  },
  connections: [
    {port: Config.get('/port'), host: '127.0.0.1', labels: 'admin' },
    // {port: 80, host: '127.0.0.1', labels: 'web' }
  ],
  registrations: [
    ...pluginsArray,
    ...routesArray
  ]
}

const store = new Confidence.Store(manifest);

module.exports.get = function (key) {
  return store.get(key, criteria);
}

module.exports.meta = function (key) {
  return store.meta(key, criteria);
}
