/**
 * simple gateway to have passed the text to the service
 * User: Jane
 * Date: 2017-06-10
 * Time: 12:00
 * 
 * Not used from 2017-07-14
 * Function has been moved to /server/controllers/bot-handler.js & /server/routes/botkit.route.js
 * by Matt.
 * 
 */

'use strict';

const Hapi = require('hapi');
const Joi = require('joi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Blipp = require('blipp');
const Pack = require('./package');
const jwt = require('jsonwebtoken');
const APIAI_KEY = '7e07ebc89af943e89e4bc60a8fec96ba'; // matt@cretve.com

const server = new Hapi.Server({ debug: { request: ['error'] } });
server.connection({ port: 3000, host: 'localhost' });

//connect to apiai
var apiai = require('apiai');

// var app = apiai("577172eae3b047ef84df5246693c4b92");  //api.ai 
var app = apiai(APIAI_KEY);

const botPostHandler = function(request, reply) {
    //console.log("rawPayload: " + request.rawPayload);
    //authorization check
    console.log("Received POST from header=" + request.headers.authorization);

    //split the authorization hearer with OAuth 2.0 bearer protocal
    var authorization = request.headers.authorization;
    var token = authorization && authorization.split(' ');

    if(token[0]!='Bearer' || token[1]!='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyNSwiaXNzIjoiaHR0cDpcL1wvc3R1ZGVudFwvYXBpXC9zdHVkZW50XC9sb2dpbiIsImlhdCI6MTQ5ODA5MjI5MiwiZXhwIjoxODEzNDUyMjkyLCJuYmYiOjE0OTgwOTIyOTIsImp0aSI6IjU5MDdhZDNhMjljODk2MjdkZTYyZGMwOWU4YjA1MGRhIn0.MxVQs-xkd8pQjWmOzKGkdscXZD4tVHcRR3MgCADxDCQ')
    {
      reply({ 
        session:request.payload.session,
        reply: 'Unauthorized authentication'
      });
      return;
    }

    console.log("Received POST from request ", ", payload = ", request.payload,  " , session=" + request.payload.session + "; text=" + (request.payload.text || 'Unknown'));

    var payload = request.payload;

    console.log(" payload =", payload, ', session=', payload.session, ', text=', payload.text);

    var options = {
          sessionId: request.payload.session
    };

    var apiRequest = app.textRequest(request.payload.text, options);
  
    apiRequest.on('response',function(response) {
      reply({ 
        session:request.payload.session,
        reply: response.result.fulfillment.speech
      });
    });

    apiRequest.on('error',function(response) {
      reply({ 
        session:request.payload.session,
        reply: 'I\'m not totally sure about that.'
      });
    });

    apiRequest.end();

}

const swaggerOptions = {
  info : {
    'title': 'Roadmap Botkit API Documentation',
    'version': Pack.version
  },
  securityDefinitions : {
    'jwt' : {
      'type': 'apiKey',
      'name': 'Authorization',
      'in': 'header'
    }
  }
};

var people = {
  56732: {
    id: 56732,
    name: 'Jen Jones',
    scope: ['a', 'b']
  }
};

var privateKey = 'hapi hapi joi joi';
var token = jwt.sign({ id: 56732 }, privateKey, { algorithm: 'HS256'});

var validate = function (decoded, request, callback) {
  if(!people[decode.id]) {
    return callback(null, false);
  }
  return callback(null, true, people[decoded.id]);
};

// server.route({
//     method: 'POST',
//     path: '/bot',
//     handler: botPostHandler
// });

server.route({
  method: 'POST',
  path: '/bot',
  config: {
    handler: botPostHandler,
    description: 'Post request to API.ai',
    notes: 'Return a response message from the API.ai',
    auth: false,
    tags: ['api']
    // validate: {
    //   payload: Joi.object({
    //     session: Joi.string(),
    //     text: Joi.string()
    //   })
    // }
  }
})

server.register([
  require('hapi-auth-jwt2'),  
  Inert,
  Vision,
  Blipp,
  {
    'register': HapiSwagger,
    'options': swaggerOptions
  }
], (err) => {
  if (err) {
    console.log(err);
  }

  server.auth.strategy('jwt', 'jwt', {
    key: privateKey,
    validateFunc: validate,
    verifyOptions: { algorithms: ['HS256'] }
  });

  server.auth.default('jwt');

  server.route([{
    method: 'GET', 
    path: '/bot',
    config: {
      auth: 'jwt',
      tags: ['api'],
      // plugins: {
      //   'hapi-swagger': {
      //     security: [{ 'jwt': [] }]
      //   }
      // },
      handler: botPostHandler
    }
  }, {
    method: 'GET',
    path: '/token',
    config: {
      auth: false,
      tags: ['api'],
      handler: function (request, reply) {
        reply({ token: token});
      }
    }
  }]);

// Start the server
server.start((err) => {
    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
 });

});

