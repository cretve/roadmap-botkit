const constants = {
  USER_ROLES: {
    USER: 'User',
    ADMIN: 'Admin',
    SUPER_ADMIN: 'SuperAdmin',
  },
  AUTH_STRATEGIES: {
    TOKEN: 'standard-jwt',
    SESSION: 'jwt-with-session',
    REFRESH: 'jwt-with-session-and-refresh-token'
  },
  PORT_ADMIN: 3003,
  PORT_WEB: 8100,
  APP_TITLE: 'Hapi Bolierplate'
};

module.exports = constants;
