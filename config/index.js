'use strict'

const Confidence =  require('confidence');
const Dotenv = require('dotenv');

const constants = require('./constants');
const emailConfig = require('./email');
const botkitHapiConfig = require('./botkit');
const authConfig =  require('./auth');

Dotenv.config({ silent: true });

const criteria = {
  env: process.env.NODE_ENV
}

const config = {
  $meta: 'This file configures the Botkit API',
  projectName: constants.APP_TITLE,
  websiteName: 'Botkit Admin',
  port: {
    $filter: 'env',
    production: process.env.PORT_ADMIN,
    $default: constants.PORT_ADMIN
  },
  constants: constants,

  /* authentication configuration */  
  authStrategy: authConfig.get('/authStrategy'),
  expirationPeriod: authConfig.get('/expirationPeriod'),
  authAttempts: authConfig.get('/authAttempts'),
  lockOutPeriod: authConfig.get('/lockOutPeriod'),
  jwtSecret: authConfig.get('/jwtSecret'),

  /* mail configuration */
  nodemailer: emailConfig.get('/nodemailer'),
  defaultEmail: emailConfig.get('/defaultEmail'),
  system: emailConfig.get('/system'),

  clientURL: {
    $filter: 'env',
    local: 'http://localhost:' + constants.PORT,
    production: 'http://localhost:' + constants.PORT,
    $default: 'http://localhost:' + constants.PORT    
  },

  /* botkit Hapi configuration */
  botkitHapiConfig : botkitHapiConfig.get('/')
}

const store = new Confidence.Store(config);

module.exports.get = function (key) {
  return store.get(key, criteria);
};

module.exports.meta = function (key) {
  return store.meta(key, criteria);
};
