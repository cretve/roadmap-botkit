'use strict'

const Confidence = require('confidence');

const constants =  require('./constants');

const criteria = {
  env: process.env.NODE_ENV
};

const emailConfig = {
  nodemailer: {
    $filter: 'env',
    local: {
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: 'matt@cretve.com',
        pass: process.env.SMTP_PASSWORD
      }
    },
    production: {
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: 'matt@cretve.com',
        pass: process.env.SMTP_PASSWORD
      }
    },
    $default: {
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: 'matt@cretve.com',
        pass: process.env.SMTP_PASSWORD
      }      
    }
  },
  defaultEmail: {
    $filter: 'env',
    local: 'matt@cretve.com',
    development: 'matt@cretve.com',
    production: 'matt@cretve.com'    
  },
  system: {
    fromAddress: {
      name: 'Botkit',
      address: 'matt@cretve.com'
    },
    toAddress: {
      name: 'Botkit',
      address: 'matt@cretve.com'
    }
  },  
};

const store = new Confidence.Store(emailConfig);

module.exports.get = function (key) {
  return store.get(key, criteria);
};

module.exports.meta = function (key) {
  return store.meta(key, criteria);
};

