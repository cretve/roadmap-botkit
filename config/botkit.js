'use strict'

const Confidence = require('confidence');

const constants =  require('./constants');

const criteria = {
  env: process.env.NODE_ENV
};

const botkitHapiConfig = {
  appTitle: constants.APP_TITLE,
  mongo: {
    URI: {
      $filter: 'env',
      local: 'mongodb://localhost/botkit',
      $default: 'mongodb://localhost/botkit'
    }
  },
  cors: {
    additionalHeaders: ['X-Total-Count', 'X-Auth-Header', 'X-Refresh-Token'],
    additionalExposedHeaders: ['X-Total-Count', 'X-Auth-Header', 'X-Refresh-Token']
  },
  absoluteModelPath: true,
  modelPath: __dirname + '/server/models',
  absoluteRoutePath: true,
  routePath: __dirname + '/server/routes',
  
  authStrategy: {
    $filter: 'env',
    local: constants.AUTH_STRATEGIES.REFRESH,
    $default: constants.AUTH_STRATEGIES.REFRESH
  },

  enableQueryValidation: {
    $filter: 'env',
    local: true,
    $default: true
  },
  enablePayloadValidation: {
    $filter: 'env',
    local: true,
    $default: true
  },
  enableResponseValidation: {
    $filter: 'env',
    local: true,
    $default: true
  },
  enableTextSearch: {
    $filter: 'env',
    local: true,
    $default: true
  },
  enableSoftDelete: {
    $filter: 'env',
    local: true,
    $default: true
  },
  generateScopes: {
    $filter: 'env',
    local: true,
    $default: true
  },
  loglevel: {
    $filter: 'env',
    local: 'DEBUG',
    $default: 'ERROR'
  }
  
};

const store = new Confidence.Store(botkitHapiConfig);

module.exports.get = function (key) {
  return store.get(key, criteria);
};

module.exports.meta = function (key) {
  return store.meta(key, criteria);
}
