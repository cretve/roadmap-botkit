'use strict'

const Confidence = require('confidence');

const constants =  require('./constants');

const criteria = {
  env: process.env.NODE_ENV
};

const authConfig = {
  authStrategy: 'jwt',
  expirationPeriod: {
    short: '10m',
    medium: '30m',
    long: '4h'
  },
  authAttempts: {
    forIp: 50,
    forIpAndUser: 7
  },
  lockOutPeriod: 30, //minutes
  jwtSecret: {
    $filter: 'env',
    production: process.env.JWT_SECRET,
    $default: 'aStrongJwtSecret-#mgtfYK@QuRV8VMM7T>WfN4;^fMVr)y'
  },  
}

const store = new Confidence.Store(authConfig);

module.exports.get = function (key) {
  return store.get(key, criteria);
};

module.exports.meta = function (key) {
  return store.meta(key, criteria);
};
